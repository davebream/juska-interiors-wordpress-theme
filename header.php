<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Juska Interiors
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'juska-interiors' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="container header">
			<div class="row">
				<div class="site-branding col-lg-4 col-md-6 col-sm-6 col-xs-10">
					<a href="<? echo site_url()?>"><img src="<? echo get_template_directory_uri().'/img/header-logo.png'; ?>"/></a>
				</div><!-- .site-branding -->
<!-- 				<div class="menu-toggle" href=".menu-toggle">
						<button class="menu-button" type="button" role="button" aria-label="Toggle Navigation" ria-controls="primary-menu" aria-expanded="false">
  							<span class="lines"></span>
						</button>
				</div> -->
				<button class="c-hamburger c-hamburger--htx menu-toggler" href=".menu-toggler" id="menu-toggler">
         			<span>toggle menu</span>
        		</button>
				<nav id="site-navigation" class="main-navigation col-lg-7 col-md-6 col-xs-12 pull-right" role="navigation">
					<?php wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu_id' => 'primary-menu',
					'container_class' => 'row',
					'container_id'	=> 'main-menu',
					'menu_class' => 'row'
					 ) ); ?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="container">
