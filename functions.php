<?php
/**
 * Juska Interiors functions and definitions
 *
 * @package Juska Interiors
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'juska_interiors_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function juska_interiors_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Juska Interiors, use a find and replace
	 * to change 'juska-interiors' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'juska-interiors', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'juska-interiors' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery'
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'juska_interiors_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // juska_interiors_setup
add_action( 'after_setup_theme', 'juska_interiors_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function juska_interiors_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'juska-interiors' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'juska_interiors_widgets_init' );

// JQUERY UPDATE

	if (!is_admin()) add_action("wp_enqueue_scripts", "bloglow_custom_jquery_cdn", 11);
	function bloglow_custom_jquery_cdn() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js', false, null);
	wp_enqueue_script('jquery');
	}

/**
 * Enqueue scripts and styles.
 */
function juska_interiors_scripts() {
	wp_enqueue_style( 'juska-interiors-style', get_stylesheet_uri() );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), null, true );

	wp_enqueue_script( 'fastclick', 'https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js', array(), null, true);

	wp_enqueue_style( 'juska-interiors-font', get_template_directory_uri() . '/fonts/museo-sans/stylesheet.css');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'juska_interiors_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * Load author bio widget.
 *
 */

class author_bio extends WP_Widget {

  	// constructor
	function author_bio() {
	  	parent::WP_Widget(false, $name = __('Author Bio', 'juska-interiorss') );
	  	add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}

	/**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts()
    {

    		wp_enqueue_style('thickbox');
    		wp_enqueue_script('media-upload');
    		wp_enqueue_script('thickbox');

        wp_enqueue_script('upload_media_widget', get_template_directory_uri() . '/js/upload-media.js', array('jquery'));
    }


  	// widget form creation
	function form($instance) {

		// Check values
		if( $instance) {
	 		$title = esc_attr($instance['title']);
	 		$image = esc_attr($instance['image']);
	 		$textarea = esc_textarea($instance['textarea']);
		} else {
			$title = '';
			$image = '';
			$textarea = '';
		}
	?>
	<p>
        <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:' ); ?></label>
        <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id( 'image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" />
        <input class="upload_image_button button button-primary" type="button" value="Upload Image" />
    </p>
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>">
			<?php _e('Title', 'juska-interiors'); ?>
		</label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('textarea'); ?>">
			<?php _e('Bio:', 'wp_widget_plugin'); ?>
		</label>
		<textarea class="widefat" id="<?php echo $this->get_field_id('textarea'); ?>" name="<?php echo $this->get_field_name('textarea'); ?>" rows="12"><?php echo $textarea; ?>
		</textarea>
	</p>
	<?php
	}

  	// update widget
	function update($new_instance, $old_instance) {
	      $instance = $old_instance;
	      // Fields
	      $instance['title'] = strip_tags($new_instance['title']);
	      $instance['image'] = strip_tags($new_instance['image']);
	      $instance['textarea'] = strip_tags($new_instance['textarea']);
	     return $instance;
	}

	// display widget
	function widget($args, $instance) {
	   	extract( $args );
	   	// these are the widget options
	   	$title = apply_filters('widget_title', $instance['title']);
	   	$textarea = $instance['textarea'];
	   	$image = $instance['image'];
	   	echo $before_widget;
	   	// Display the widget
	   	echo '<div class="widget-text wp_widget_plugin_box row">';

	   	// Check if image is set
	   	if ( $image ) {
	   		echo '<img src="'.$image.'" class="author-image col-lg-12 col-md-6 col-sm-4 col-xs-4"/>';
	   	}

	   	echo '<div class="col-lg-12 col-md-6 col-sm-8 col-xs-8">';
	   		echo '<div class="author-bio-body">';

	   	// Check if title is set
	   	if ( $title ) {
	   	   echo '<h5 class="author-bio-title">'.$title.'</h5>';
	   	}

	   	// Check if textarea is set
	   	if( $textarea ) {
	     	echo '<p class="wp_widget_plugin_textarea">'.$textarea.'</p>';
	   	}
	   		echo '</div>';
	   	echo '</div>';
	   	echo '</div>';
	   	echo $after_widget;
	}
}


add_action('widgets_init', create_function('', 'return register_widget("author_bio");'));

// Slim Jetpack Content Width
$content_width = 930;

