<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Juska Interiors
 */
?>
</div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer container footer" role="contentinfo">
			<div class="row">
				<div class="site-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php _e( 'Created by ', 'juska-interiors' );?>
					<a href="http://underscores.me/" rel="designer">
						<? _e('Bream Collective', 'juska-interiors');?>
					</a>
				</div><!-- .site-info -->
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
