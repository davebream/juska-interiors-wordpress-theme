var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var paths = {
  js: ['./js/*.js'],
  sass: ['./sass/**/*.scss']
};

gulp.task('sass', function () {
    return gulp.src('./sass/style.scss')
    	// .pipe(sourcemaps.init())
        .pipe(sass())
        // .pipe(sourcemaps.write()
        .pipe(gulp.dest('./'));
});

gulp.task('js', function () {
	gulp.src('./js/front/*.js')
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./js/'));
});

gulp.task('watch', function() {
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.sass, ['sass']);
});