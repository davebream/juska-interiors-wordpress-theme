$(document).ready(function(){

    $('.menu-toggler').sidr({
      name: 'mobile-menu',
      source: '#main-menu',
      side: 'right'
    });

});

(function() {

  "use strict";

  var toggle = document.getElementById("menu-toggler");
  toggleHandler(toggle);

  function toggleHandler(toggle) {
    toggle.addEventListener( "click", function(e) {
      e.preventDefault();
      (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
    });
  }

  // function toggleActivation() {
  // 	var this = $("#menu-toggler");
  // 	if (this.hasClass('is-active')){
  // 		this.removeClass('is-active');
  // 	} else {
  // 		this.addClass('is-active');
  // 	}
  // }

  (function( $ ){
   $.fn.toggleActivationOnSwipe = function(swipeDirection) {
	   	switch (swipeDirection) {
	   		case "right":
	   			this.removeClass('is-active');
	   			break;
	   		case "left":
	   			this.addClass('is-active');
	   			break;
	   		default:
	   			return false;
	   	}
   	return this;
   };
})( jQuery );

})();

$(function() {
    FastClick.attach(document.body);
});


$(window).touchwipe({

        wipeLeft: function() {
          // Close
          $.sidr('open', 'mobile-menu');
          $("#menu-toggler").toggleActivationOnSwipe("left");
        },
        wipeRight: function() {
          // Open
          $.sidr('close', 'mobile-menu');
          $("#menu-toggler").toggleActivationOnSwipe("right");
        },
        preventDefaultEvents: false
});