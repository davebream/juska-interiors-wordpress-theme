<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package Juska Interiors
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function juska_interiors_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'juska_interiors_jetpack_setup' );
